ansible-node-yarn
============

Installs yarn and nodejs from the upstream apt repositories.

Requirements
------------

* Debian

Role Variables
--------------

```
# Set the version of Node.js to install ("6.x", "8.x", "9.x", "10.x", etc.).
# Version numbers from Nodesource:
# https://github.com/nodesource/distributions
nodejs_version: "10.x"
```


Example Playbook
----------------

    - hosts: servers
      roles:
        - deb-baseline

Testing
-------

    molecule test

License
-------

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

Author Information
------------------

```
Abel Luck <abel@guardianproject.info>
Guardian Project https://guardianproject.info
```

